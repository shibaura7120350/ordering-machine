import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JTextField textField1;
    private JButton maguroButton;
    private JButton amaebiButton;
    private JButton ikaButton;
    private JButton anagoButton;
    private JButton salmonButton;
    private JButton hotateButton;
    private JTextArea textMessage;
    private JButton checkOut;
    private JTextArea yenMessage;
    private JLabel totalPrice;
    private int total = 0;
    public SimpleFoodOrderingMachine() {
        maguroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Maguro",100);}
        });
        amaebiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Amaebi", 110);}
        });
        ikaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Ika", 120);}
        });
        anagoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Anago", 130);}
        });
        salmonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Salmon", 140);}
        });
        hotateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Hotate", 150);}
        });

        checkOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?",
                        "Check out",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0){
                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you!",
                            "Success!",
                            JOptionPane.INFORMATION_MESSAGE
                    );
                    textMessage.setText("");
                    yenMessage.setText("");
                    total = 0;
                }
            }
        });
    }

    void order(String foodName, int price){
        String[] options = {"1", "2", "3", "4", "5"};
        String confirmation = (String)JOptionPane.showInputDialog(
                null,
                "How many plates of "+foodName+" would you like to order?",
                "Order Confirmation",
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[0]
        );

        if(confirmation != null){
            int count = Integer.parseInt(confirmation);
            JOptionPane.showMessageDialog(
                    null,
                    "Order for "+foodName+" received",
                    "Message",
                    JOptionPane.INFORMATION_MESSAGE
            );
            String currentText = textMessage.getText();
            textMessage.setText(currentText + foodName + " ×" + count + "\n");
            total += price * count;
            yenMessage.setText(total + " yen");
        }else{
            JOptionPane.showMessageDialog(
                    null,
                    "The order has been canceled",
                    "Cancel",
                    JOptionPane.INFORMATION_MESSAGE
            );
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        //画面サイズ変更と固定
        frame.setSize(1000,500);
        frame.setResizable(false);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
